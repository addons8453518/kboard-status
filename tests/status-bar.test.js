import StatusBar from "../src/extension/status-bar";

describe("StatusBar", () => {
  it("should create a status bar with icon and label", () => {
    const bar = new StatusBar("test-icon", "Initial Text");
    expect(bar.icon).toBeDefined();
    expect(bar.label).toBeDefined();
    expect(bar.label.text).toBe("Initial Text");
  });

  it("should update the label text", () => {
    const bar = new StatusBar("test-icon", "Initial Text");
    bar.setText("Updated Text");
    expect(bar.label.text).toBe("Updated Text");
  });
});
