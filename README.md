# KBoard Status

Status bar for Keychron keyboard battery.

## Build extension

Will generate a dist folder with the extenion.

```
npm run build
```

### Install extension

Will install the extension on your gnome-shell directly (requies gnome desktop environment)

```
npm run install
```
