class StatusBar {
  constructor(iconName, labelText) {
    this.icon = new St.Icon({ icon_name: iconName });
    this.label = new St.Label({ text: labelText });

    const box = new St.BoxLayout({ style_class: "panel-status-menu-box" });
    box.add_child(this.icon);
    box.add_child(this.label);

    this.actor = box;
  }

  setText(text) {
    this.label.set_text(text);
  }

  appendTo(container) {
    container.add_child(this.actor);
  }
}
